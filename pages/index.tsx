import Head from 'next/head'

import About from '../components/About';
import Splash from '../components/Splash';
import Contact from '../components/Contact';

export default function Home() {
  return <div>
    <Head>
      <title>Softaloossi Oy</title>
      <meta name="description" content="Softaloossi Oy is a company based in Finland that makes software." />
      <link rel="icon" href="favicon.svg" />
    </Head>

    <div className="App">
      <h1 style={{display:'none'}}>Softaloossi Oy</h1>
      
      <section className='scroll-1'>
        <Splash />
      </section>
      <section className='scroll-2'>
        <About />
      </section>
      <section className='scroll-3'>
        <Contact />
      </section>
    </div>
  </div>
}
