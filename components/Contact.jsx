export default function Contact() {
    return <div className="contact">
        <h1>Contact</h1>
        <p>
            We pride ourselves on our dedication to customer service and 
            satisfaction. If you have any questions or need assistance with 
            BettyLabra, we are here ready to help. Thank you for 
            considering Softaloossi Oy for your software needs.
        </p>

        <li className="contact-info">
            <ul>Softaloossi Oy</ul>
            <ul>3294299-1</ul>
            <ul>Rami Ojala</ul>
            <ul>firstname.lastname@softaloossi.fi</ul>
        </li>
    </div>
}