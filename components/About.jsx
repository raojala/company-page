export default function About() {
    return <div className="About">

        <h1>About us</h1>
        <p>
            Softaloossi Oy is a one-man software development company based in 
            Finland, founded on the values of openness, freedom, and time. We 
            proudly serve a global client base, offering innovative software 
            solutions that improve efficiency and streamline processes. Our 
            goal is to provide reliable and user-friendly products that help 
            our clients achieve success.
        </p>
        <p>
            At Softaloossi Oy, we believe in the power of technology to make a
            positive impact in the world. That&apos;s why we&apos;re committed to creating 
            software that is not only effective, but also easy to use and 
            accessible to everyone.
        </p>

        <h2>BettyLabra</h2>
        <p>
            Our goal is for our flagship product, BettyLabra, to be the go-to 
            tool used by laboratories around the world to report against 
            existing standards. With its intuitive interface and comprehensive 
            reporting capabilities, we aim to provide professionals with a 
            reliable and efficient solution for their reporting needs.
        </p>
    </div>
}